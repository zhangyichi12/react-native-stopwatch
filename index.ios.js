/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import formatTime from 'minutes-seconds-milliseconds';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    TouchableHighlight
} from 'react-native';

class Stopwatch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timeElapsed: "",
            running: false,
            startTime: "",
            laps: []
        }

        this.handleStartPress = this.handleStartPress.bind(this);
        this.handleLapPress = this.handleLapPress.bind(this);
    }

    render() {
        return (
            <View style={ styles.container }>
                <View style={[ styles.header, this.border('yellow') ]}>{/* Yellow */}
                    <View style={[ this.border('red'), styles.timerWrapper ]}>{/* Red */}
                        <Text style={ styles.timer }>
                            { formatTime(this.state.timeElapsed) }
                        </Text>
                    </View>
                    <View style={[ this.border('green'), styles.buttonWrapper ]}>{/* Green */}
                        { this.startStopButton() }
                        { this.lapButton() }
                    </View>
                </View>

                <View style={[ styles.footer, this.border('blue') ]}>{/* Blue */}
                    { this.state.laps.map((time, index) => {
                        return (
                            <View key={ index } style={ styles.lap }>
                                <Text style={ styles.lapText} >
                                    Lap #{ index + 1 }
                                </Text>
                                <Text style={ styles.lapText }>
                                    { formatTime(time) }
                                </Text>
                            </View>
                        )
                    })}
                </View>
            </View>
        );
    }

    startStopButton() {
        return (
            <TouchableHighlight
                underlayColor = 'gray'
                onPress={ this.handleStartPress }
                style={ [styles.button, (this.state.running ? styles.stopButton : styles.startButton)] }
            >
                <Text>
                    { this.state.running ? 'Stop' : 'Start' }
                </Text>
            </TouchableHighlight>
        )
    }
    handleStartPress() {
        if(this.state.running) {
            clearInterval(this.interval);
            this.setState({running: false});
            return ;
        }

        this.setState({ startTime: new Date() })
        this.interval = setInterval(() => {
            this.setState({
                timeElapsed: new Date() - this.state.startTime,
                running: true
            });
        }, 30 )
    }

    lapButton() {
        return (
            <TouchableHighlight
                underlayColor = 'gray'
                onPress={ this.handleLapPress }
                style={ styles.button }
            >
                <Text>
                    Lap
                </Text>
            </TouchableHighlight>
        )
    }
    handleLapPress() {
        const lap = this.state.timeElapsed;
        this.setState({
            startTime: new Date(),
            laps: this.state.laps.concat([lap]) //not using push, because 'state' is read-only
        });
    }

    border(color) {
        return {
            borderColor: color,
            borderWidth: 4
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, // fill the entire the screen
        alignItems: 'stretch'
    },
    header: {   // Yellow
        flex: 1
    },
    footer: {   // Blue
        flex: 1
    },
    timerWrapper: { // Red
        flex: 5, // get 5 / 8 of the available space
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonWrapper: { // Green
        flex: 3, // get 3 / 8 of the available space
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    timer: {
        fontSize: 60
    },
    button: {
        borderWidth: 2,
        borderRadius: 50,
        height: 100,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    startButton: {
        borderColor: 'green'
    },
    stopButton: {
        borderColor: 'red'
    },
    lap: {
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    lapText: {
        fontSize: 30
    }
});

AppRegistry.registerComponent('stopwatch', () => Stopwatch);
